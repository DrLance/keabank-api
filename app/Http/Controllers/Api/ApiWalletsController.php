<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Transfer;
use App\Models\Wallet;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ApiWalletsController extends Controller
{

    use ApiResponser;

    public function getWallets(Request $request)
    {
        $wallets = Category::with(['wallets'])->get();

        return $this->success($wallets,'OK');
    }

    public function sendToWallet(Request $request) {

        try {

            $transfer = new Transfer();

            $transfer->from    = $request->wallet_id;
            $transfer->to      = $request->to_wallet_id;
            $transfer->user_id = auth()->id();
            $transfer->amount  = $request->amount;
            $transfer->type    = "sendTo";
            $transfer->status  = "successes";

            $transfer->save();

            $walletFrom = Wallet::where('id', $request->wallet_id)->first();
            $walletTo   = Wallet::where('id', $request->to_wallet_id)->first();

            if ($walletFrom->currency->name === 'USD' && $walletTo->currency->name === 'EUR') {
                $walletFrom->balance -= $transfer->amount;
                $walletTo->balance   += ($transfer->amount * 0.83);
            } elseif ($walletFrom->currency->name === 'EUR' && $walletTo->currency->name === 'USD') {
                $walletFrom->balance -= $transfer->amount;
                $walletTo->balance   += ($transfer->amount * 1.21);
            } else {
                $walletFrom->balance -= $transfer->amount;
                $walletTo->balance   += ($transfer->amount * 1.21);
            }


            $walletTo->save();
            $walletFrom->save();

            return $this->success([], 'OK');
        } catch (\Exception $e) {
            return $this->error($e->getMessage(),400,[]);
        }
    }

}
