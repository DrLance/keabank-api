<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiAuthController extends Controller
{
    use ApiResponser;

    public function login(Request $request)
    {
        try {
            $request->validate([
              'phone' => 'required',
            ]);


            $phone = str_replace([' ', '(', ')', '-', '+'], '', $request->phone);

            if(!Auth::attempt(['email' => $phone . "@coderman.ru", 'password' => $request->password])) {
                throw new \Exception('Login Failed');
            }

            $user = Auth::user();

            $authToken = $user->createToken('auth-token')->plainTextToken;

            return $this->success([
              'access_token' => $authToken,
            ], 'OK');

        } catch (\Exception $e) {
            return $this->error($e->getMessage(), 401, []);
        }

    }

    public function register(Request $request)
    {
        try {
            $request->validate([
              'phone' => 'required',
            ]);

            $phone = str_replace([' ', '(', ')', '-', '+'], '', $request->phone);

            $user = User::create([
              'name'     => $phone,
              'email'    => $phone . '@coderman.ru',
              'password' => bcrypt($request->password),
              'phone'    => $phone,
            ]);

            event(new Registered($user));

            $authToken = $user->createToken('auth-token')->plainTextToken;
            $user->access_token = $authToken;

            return $this->success($user, 'OK');
        } catch (\Exception $e) {
            \Log::debug($e->getMessage());

            return $this->error('Server Error', 401, []);
        }

    }

    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();

        return $this->success([], 'OK');

    }
}
