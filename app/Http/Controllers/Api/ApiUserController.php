<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ApiUserController extends Controller
{
    use ApiResponser;

    public function updateUser(Request $request) {
        $user = auth()->user();

        $email = $request->email;
        $phone = $request->phone;

        $user->email = $email;
        $user->phone = $phone;


        $user->save();

        return $this->success($user,'OK');

    }

    public function uploadAvatar(Request $request) {
        $user = auth()->user();

        $file = $request->file('avatar');
        $path = $file->store("avatar", 'public');

        $user->avatar_url = Storage::url($path);

        $user->save();

        return $this->success($user,'OK');

    }

}
