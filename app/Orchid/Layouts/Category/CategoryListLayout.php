<?php

namespace App\Orchid\Layouts\Category;

use App\Models\Category;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CategoryListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'categories';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
          TD::make('name', 'Name')
            ->render(function (Category $category) {
                return Link::make($category->name)
                           ->route('platform.category.edit', $category);
            }),

          TD::make('created_at', 'Created'),
          TD::make('updated_at', 'Last edit'),
        ];
    }
}
