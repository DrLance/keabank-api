<?php

namespace App\Orchid\Layouts\Wallet;

use App\Models\Currency;
use App\Models\Wallet;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class WalletListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'wallets';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
          TD::make('address', 'Address')
            ->render(function (Wallet $wallet) {
                return Link::make($wallet->address)
                           ->route('platform.wallet.edit', $wallet);
            }),

          TD::make('category_id', 'Category')->render(function (Wallet $wallet) {
              return $wallet->category->name;
          }),
          TD::make('user_id', 'User')->render(function (Wallet $wallet){
              if($wallet->user) {
                  return $wallet->user->email;
              }

              return  '';
          }),
          TD::make('currency_id', 'Currency')->render(function (Wallet $wallet){
              return $wallet->currency->name;
          }),
          TD::make('balance', 'Balance'),

          TD::make('created_at', 'Created'),
          TD::make('updated_at', 'Last edit'),
        ];
    }
}
