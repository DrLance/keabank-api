<?php

namespace App\Orchid\Layouts\Currency;

use App\Models\Category;
use App\Models\Currency;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;

class CurrencyListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'currencies';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
          TD::make('name', 'Name')
            ->render(function (Currency $currency) {
                return Link::make($currency->name)
                           ->route('platform.currency.edit', $currency);
            }),

          TD::make('code', 'Code'),
          TD::make('icon', 'Icon')
            ->width(36)
            ->render(function (Currency $currency) {

                if ($currency->icon) {
                    return "<img src='{$currency->icon}' alt='sample' class='mw-100 d-block img-fluid'>";
                }

                return '';

            }),
          TD::make('icon_type', 'Icon Type'),
          TD::make('type', 'Type'),
          TD::make('country_icon', 'Country Icon')
            ->width(36)
            ->render(function (Currency $currency) {

                if ($currency->country_icon) {
                    return "<img src='{$currency->country_icon}' alt='sample' class='mw-100 d-block img-fluid'>";
                }

                return '';

            }),
          TD::make('rate', 'Rate'),
          TD::make('created_at', 'Created'),
          TD::make('updated_at', 'Last edit'),
        ];
    }
}
