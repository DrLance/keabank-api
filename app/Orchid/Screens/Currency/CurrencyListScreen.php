<?php

namespace App\Orchid\Screens\Currency;

use App\Models\Currency;
use App\Orchid\Layouts\Currency\CurrencyListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class CurrencyListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'CurrencyListScreen';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'CurrencyListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
          'currency'   => Currency::find(1),
          'currencies' => Currency::paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
          Link::make('Create new')
              ->icon('pencil')
              ->route('platform.currency.edit')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
          CurrencyListLayout::class,
        ];
    }
}
