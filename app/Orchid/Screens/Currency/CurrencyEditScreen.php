<?php

namespace App\Orchid\Screens\Currency;

use App\Models\Category;
use App\Models\Currency;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class CurrencyEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'CurrencyEditScreen';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'CurrencyEditScreen';

    public $exists;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Currency $currency): array
    {
        $this->exists = $currency->exists;

        if ($this->exists) {
            $this->name = 'Edit Currency';
        }

        $currency->load('attachment');

        return [
          'currency' => $currency,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
          Button::make('Create')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(! $this->exists),

          Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

          Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
          Layout::rows([
            Input::make('currency.name')->title('Name')->required(),
            Input::make('currency.code')->title('Code')->required(),
            Input::make('currency.rate')->title('Rate')->required(),
            Picture::make('currency.icon')
                   ->targetRelativeUrl()->required(),
            Input::make('currency.icon_type')->title('Icon Type')->required(),
            Input::make('currency.type')->title('type')->required(),
            Picture::make('currency.country_icon')
                   ->targetRelativeUrl()    ,
          ]),
        ];
    }

    /**
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Currency $currency, Request $request)
    {
        $currency->fill($request->get('currency'))->save();

        $currency->attachment()->sync(
          $request->input('currency.attachment', [])
        );

        Alert::info('You have successfully created or updated.');

        return redirect()->route('platform.currency.list');
    }

    /**
     *
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Currency $currency)
    {
        $currency->delete();

        Alert::info('You have successfully deleted the category.');

        return redirect()->route('platform.currency.list');
    }
}
