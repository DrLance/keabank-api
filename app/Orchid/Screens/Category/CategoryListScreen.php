<?php

namespace App\Orchid\Screens\Category;

use App\Models\Category;
use App\Orchid\Layouts\Category\CategoryListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class CategoryListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'CategoryListScreen';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'CategoryListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
          'category'   => Category::find(1),
          'categories' => Category::paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
          Link::make('Create new')
              ->icon('pencil')
              ->route('platform.category.edit')
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
          CategoryListLayout::class
        ];
    }
}
