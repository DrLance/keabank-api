<?php

namespace App\Orchid\Screens\Wallet;

use App\Models\Wallet;
use App\Orchid\Layouts\Wallet\WalletListLayout;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class WalletListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'WalletListScreen';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'WalletListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
          'wallet'  => Wallet::find(1),
          'wallets' => Wallet::paginate(),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
          Link::make('Create')
              ->icon('pencil')
              ->route('platform.wallet.edit'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
          WalletListLayout::class,
        ];
    }
}
