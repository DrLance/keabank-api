<?php

namespace App\Orchid\Screens\Wallet;

use App\Models\Category;
use App\Models\Currency;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Facades\Layout;

class WalletEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'WalletEditScreen';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'WalletEditScreen';

    public $exists;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Wallet $wallet): array
    {
        $this->exists = $wallet->exists;

        if ($this->exists) {
            $this->name = 'Edit Currency';
        }

        return [
          'wallet' => $wallet,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
          Button::make('Create')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(! $this->exists),

          Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

          Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
          Layout::rows([
            Input::make('wallet.address')->title('Address')->required(),
            Relation::make('wallet.category_id')->fromModel(Category::class, 'name')->title('Category')->required(),
            Relation::make('wallet.currency_id')->fromModel(Currency::class, 'name')->title('Currency')->required(),
            Relation::make('wallet.user_id')->fromModel(User::class, 'email')->title('User'),
            Input::make('wallet.balance')
                 ->title('Balance')
                 ->mask([
                   'numericInput' => true,
                 ])
          ])
        ];
    }

    public function createOrUpdate(Wallet $wallet, Request $request)
    {
        $wallet->fill($request->get('wallet'))->save();

        Alert::info('You have successfully created.');

        return redirect()->route('platform.wallet.list');
    }

    /**
     *
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Wallet $wallet)
    {
        $wallet->delete();

        Alert::info('You have successfully deleted.');

        return redirect()->route('platform.wallet.list');
    }
}
