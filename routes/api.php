<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', [\App\Http\Controllers\Api\ApiAuthController::class, 'register'])->name('api.register');
Route::post('/login', [\App\Http\Controllers\Api\ApiAuthController::class, 'login'])->name('api.register');

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/me', function(Request $request) {
        return response()->json(auth()->user());
    });

    Route::post('/wallets', [\App\Http\Controllers\Api\ApiWalletsController::class, 'getWallets']);
    Route::post('/wallets/send', [\App\Http\Controllers\Api\ApiWalletsController::class, 'sendToWallet']);

    Route::post('/user/update', [\App\Http\Controllers\Api\ApiUserController::class, 'updateUser']);
    Route::post('/user/upload/avatar', [\App\Http\Controllers\Api\ApiUserController::class, 'uploadAvatar']);

    Route::post('/logout', [\App\Http\Controllers\Api\ApiAuthController::class, 'logout']);
});
